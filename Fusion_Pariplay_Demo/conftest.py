# content of conftest.py


def pytest_addoption(parser):
    parser.addoption(
        "--evi", action="store", help="Name of the conf file to pass to test functions"
    )
    parser.addoption(
        "--evi_games",
        action="store",
        default="all",
        required=False,
        help="Number of games to pass to " "test functions",
    )
