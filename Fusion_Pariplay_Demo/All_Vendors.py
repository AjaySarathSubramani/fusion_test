import logging
import random
import time
import pytest
import pytest_check as check
import Common_Variables as Common
import Request_Body as VendorEVI
import module_Main as Main
import allure

workers = ["gw%d" % i for i in range(0, 10)]
Players = Common.get_Players()


# Accounts = [('%d' % i, 'ta%d' % j, 'ta%d' % j) for i, j in zip (range(20451, 20461), range(1, 11))]

@pytest.hookimpl(trylast=True)
def pytest_configure(config):
    allure.title('QA Automation Tests')


@pytest.fixture()
def user_account(worker_id):
    """use a different account in each xdist worker"""
    if worker_id in workers:
        return Players[workers.index(worker_id)]


def pytest_generate_tests(metafunc):
    evi = metafunc.config.getoption("--evi")
    evi_games = metafunc.config.getoption("--evi_games")

    # only parametrize tests with the correct parameters
    if evi and evi_games:
        metafunc.parametrize(
            "game_code, V_User, V_Pwd, Debit, Credit", Common.getconfig(evi, evi_games)
        )


# ----------------------------------------------------------------------------------------------------------------------
# Test Case Name   : test_Debit_Credit_VendorEVI
# Test Description : To reduce and add the value to balance, Validating the Actual value with Expected Balance.
# API's            : Launch Game, Create Token, Authentication, Credit, Debit, End Game
# ----------------------------------------------------------------------------------------------------------------------

@allure.suite('Debit - Credit Test Case')
@pytest.mark.EVI_Sanity
@pytest.mark.EVI_Functional
@pytest.mark.flaky(reruns=1, reruns_delay=2)
def test_Debit_Credit_VendorEVI(game_code, V_User, V_Pwd, Debit, Credit, user_account):
    vendor_user = V_User
    vendor_pwd = V_Pwd
    debit_amount = Debit
    credit_amount = Credit
    if user_account is not None:
        (p_id, user, pwd) = user_account
    else:
        p_id, user, pwd = 20451, 'ta1', 'ta1'
    # p_id = str(p_id)

    token, url, client_token = Main.Response_GameLaunch(
        Common.LaunchGame_Url, VendorEVI.LaunchGame_Request, game_code, p_id
    )
    Main.Response_CreateToken(
        Common.CreateToken_Url,
        VendorEVI.CreateToken_Request,
        game_code,
        token,
        p_id,
        user,
        pwd,
    )
    player_id, auth_balance, resp_token = Main.Response_Authentication(
        Common.Auth_Url,
        VendorEVI.Auth_Request,
        game_code,
        client_token,
        vendor_user,
        vendor_pwd,
    )

    if game_code[:3] == "SM_":
        game_code = Common.SpinMatic(game_code)
    else:
        pass
    game_code = game_code.split("_", 1)[1]
    round_id, trans_id = str(hash(resp_token)), str(time.time()) + str(p_id)
    debit_balance, debit_transaction_id = Main.Response_Debit(
        Common.Debit_Url,
        VendorEVI.Debit_Request,
        game_code,
        resp_token,
        player_id,
        trans_id,
        round_id,
        vendor_user,
        vendor_pwd,
    )
    actual_debit = auth_balance - debit_balance
    check.equal(actual_debit, debit_amount, "Fail: Incorrect Debit Balance")
    credit_balance, credit_transaction_id = Main.Response_Credit(
        Common.Credit_Url,
        VendorEVI.Credit_request_EVI,
        game_code,
        resp_token,
        trans_id,
        player_id,
        str(time.time()) + str(p_id),
        round_id,
        vendor_user,
        vendor_pwd,
    )
    actual_credit = credit_balance - debit_balance
    check.equal(actual_credit, credit_amount, "Fail: Incorrect Credit Balance")


# ----------------------------------------------------------------------------------------------------------------------
# Test Case Name   :test_Debit_Cancel_VendorEVI
# Test Description :To reduce and Cancel transaction ,Validating the Actual value with Expected Balance .
# API's            :Launch Game,Create Token,Authentication,  Debit ,Cancel Transaction
# ----------------------------------------------------------------------------------------------------------------------
@allure.suite('Debit - Cancel Test Case')
@pytest.mark.EVI_Functional
@pytest.mark.flaky(reruns=1, reruns_delay=2)
def test_Debit_Cancel_VendorEVI(game_code, V_User, V_Pwd, Debit, Credit, user_account):
    vendor_user = V_User
    vendor_pwd = V_Pwd
    debit_amount = Debit
    credit_amount = Credit
    if user_account is not None:
        (p_id, user, pwd) = user_account
    else:
        p_id, user, pwd = 20451, 'ta1', 'ta1'

    token, url, client_token = Main.Response_GameLaunch(
        Common.LaunchGame_Url, VendorEVI.LaunchGame_Request, game_code, p_id
    )
    Main.Response_CreateToken(
        Common.CreateToken_Url,
        VendorEVI.CreateToken_Request,
        game_code,
        token,
        p_id,
        user,
        pwd,
    )
    player_id, auth_balance, resp_token = Main.Response_Authentication(
        Common.Auth_Url,
        VendorEVI.Auth_Request,
        game_code,
        client_token,
        vendor_user,
        vendor_pwd,
    )
    if game_code[:3] == "SM_":
        game_code = Common.SpinMatic(game_code)
    else:
        pass
    game_code = game_code.split("_", 1)[1]
    round_id, trans_id = str(hash(resp_token)), str(time.time()) + str(p_id)
    debit_balance, debit_transaction_id = Main.Response_Debit(
        Common.Debit_Url,
        VendorEVI.Debit_Request,
        game_code,
        resp_token,
        player_id,
        trans_id,
        round_id,
        vendor_user,
        vendor_pwd,
    )
    actual_debit = auth_balance - debit_balance
    check.equal(actual_debit, debit_amount, "Fail: Incorrect Debit Balance")
    ref_t_id = trans_id
    cancel_transaction_balance, cancel_transaction_id = Main.Response_CancelTransaction(
        Common.CancelTransaction_Url,
        VendorEVI.CancelTransaction_Request_EVI,
        ref_t_id,
        game_code,
        resp_token,
        player_id,
        str(time.time()) + str(p_id),
        round_id,
        vendor_user,
        vendor_pwd,
    )
    check.equal(
        cancel_transaction_balance,
        auth_balance,
        "Fail : Incorrect Balance after Cancel Transaction",
    )


# ----------------------------------------------------------------------------------------------------------------------
# Test Case Name   :test__Multiple_Debit_Cancel_fusion
# Test Description :To reduce and Cancel transaction ,Validating the Actual value with Expected Balance .
# API's            :Launch Game,Create Token,Authentication,  Debit ,Cancel Transaction
# ----------------------------------------------------------------------------------------------------------------------
@allure.suite('Multiple Debit - Cancel Test Case')
@pytest.mark.EVI_Functional
@pytest.mark.flaky(reruns=1, reruns_delay=2)
def test_Multiple_Debit_Cancel_VendorEVI(
        game_code, V_User, V_Pwd, Debit, Credit, user_account
):
    vendor_user = V_User
    vendor_pwd = V_Pwd
    debit_amount = Debit
    credit_amount = Credit
    if user_account is not None:
        (p_id, user, pwd) = user_account
    else:
        p_id, user, pwd = 20451, 'ta1', 'ta1'

    token, url, client_token = Main.Response_GameLaunch(
        Common.LaunchGame_Url, VendorEVI.LaunchGame_Request, game_code, p_id
    )
    Main.Response_CreateToken(
        Common.CreateToken_Url,
        VendorEVI.CreateToken_Request,
        game_code,
        token,
        p_id,
        user,
        pwd,
    )
    player_id, auth_balance, resp_token = Main.Response_Authentication(
        Common.Auth_Url,
        VendorEVI.Auth_Request,
        game_code,
        client_token,
        vendor_user,
        vendor_pwd,
    )
    if game_code[:3] == "SM_":
        game_code = Common.SpinMatic(game_code)
    else:
        pass
    game_code = game_code.split("_", 1)[1]
    ref_t_id = []
    for i in range(5):
        round_id, trans_id = str(hash(resp_token)), str(time.time()) + str(p_id)
        ref_t_id.append(trans_id)
        time.sleep(0.1)
        debit_balance, debit_transaction_id = Main.Response_Debit(
            Common.Debit_Url,
            VendorEVI.Debit_Request,
            game_code,
            resp_token,
            player_id,
            trans_id,
            round_id,
            vendor_user,
            vendor_pwd,
        )
    consolidate_actual_debit = auth_balance - debit_balance
    check.equal(
        consolidate_actual_debit,
        debit_amount * 5,
        "Fail : Consolidate Debit Balance Mismatch",
    )
    logging.info("Debit_Transaction_Id : " + str(ref_t_id))
    random_num = random.choice(ref_t_id)
    logging.info("random_num " + str(random_num))
    cancel_transaction_balance, cancel_transaction_id = Main.Response_CancelTransaction(
        Common.CancelTransaction_Url,
        VendorEVI.CancelTransaction_Request_EVI,
        random_num,
        game_code,
        resp_token,
        player_id,
        str(time.time()) + str(p_id),
        round_id,
        vendor_user,
        vendor_pwd,
    )
    check.equal(
        debit_balance,
        cancel_transaction_balance - debit_amount,
        "Fail : Incorrect consolidate Balance after Cancel Transaction",
    )
