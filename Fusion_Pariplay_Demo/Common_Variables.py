import json

# from itertools import cycle
from os import listdir as ld

# my_config = open('EVI_Common_Config.json', 'r')
my_config = open("configs/Main_Config.json", "r")
jsonData = my_config.read()
main_object = json.loads(jsonData)

D = main_object["Players"]
L = list((D.keys()))
for i in range(len(D)):
    globals()[f"P{i + 1}"] = D[L[i]]["Id"]
    globals()[f"P{i + 1}_User"] = D[L[i]]["User"]
    globals()[f"P{i + 1}_Pwd"] = D[L[i]]["Password"]
Base_Url = main_object["Base_Url"]
Launch_Url = main_object["Launch_Url"]
Operator_Url = main_object["Operator_Url"]
Home_Url = main_object["HomeUrl"]
Launch_URI = main_object["Launch_URI"]
CreateToken_URI = main_object["CreateToken_URI"]
Auth_URI = main_object["Auth_URI"]
GetBalance_URI = main_object["GetBalance_URI"]
Credit_URI = main_object["Credit_URI"]
Debit_URI = main_object["Debit_URI"]
CancelTransaction_URI = main_object["CancelTransaction_URI"]
EndGame_URI = main_object["EndGame_URI"]

# Url Constructor
LaunchGame_Url = Launch_Url + Launch_URI
CreateToken_Url = Operator_Url + CreateToken_URI

Auth_Url = Base_Url + "EVI" + Auth_URI
GetBalance_Url = Base_Url + "EVI" + GetBalance_URI
Credit_Url = Base_Url + "EVI" + Credit_URI
Debit_Url = Base_Url + "EVI" + Debit_URI
CancelTransaction_Url = Base_Url + "EVI" + CancelTransaction_URI
EndGame_Url = Base_Url + "EVI" + EndGame_URI


def get_Players(games="all"):
    players = []
    for j in range(len(D)):
        players.append(
            (
                globals()[f"P{j + 1}"],
                globals()[f"P{j + 1}_User"],
                globals()[f"P{j + 1}_Pwd"],
            )
        )
    return players


configs = []


# Getting config file from the respective EVI for Sanity
def getconfig(evi, evi_games):
    if evi == "all":
        # For all vendors and any number of games
        file = ld("configs")
        file.remove("Main_Config.json")
        for f in file:
            print(f)
            with open("configs/" + f, "r") as file:
                data = file.read()
                obj = json.loads(data)
                # vendor_name = obj['Name']
                g_codes = obj["Game_Codes"][: int(evi_games)]
                v_user = obj["Hub_Vendor_User"]
                v_pwd = obj["Hub_Vendor_Password"]
                debit = obj["Debit_Amount"]
                credit = obj["Credit_Amount"]

                configs.extend(
                    [
                        (code, v_user, v_pwd, debit, credit)
                        for code in g_codes
                        if (code, v_user, v_pwd, debit, credit) not in configs
                    ]
                )
        return configs
    else:
        # For single vendor and any number of games
        with open("configs/" + str(evi) + "_EVI_Config.json") as file:
            data = file.read()
            obj = json.loads(data)
            # vendor_name = obj['Name']
            g_codes = obj["Game_Codes"][: int(evi_games)]
            v_user = obj["Hub_Vendor_User"]
            v_pwd = obj["Hub_Vendor_Password"]
            debit = obj["Debit_Amount"]
            credit = obj["Credit_Amount"]

            configs.extend(
                [
                    (code, v_user, v_pwd, debit, credit)
                    for code in g_codes
                    if (code, v_user, v_pwd, debit, credit) not in configs
                ]
            )
        return configs


def SpinMatic(game_code):
    with open("configs/SM_Spinmatic_EVI_Config.json") as file:
        data = file.read()
        obj = json.loads(data)
        codes1 = obj["Game_Codes"]
        codes2 = obj["External_ID"]
        game_code = "SM_" + codes2[codes1.index(game_code)]
        return game_code
