LaunchGame_Request = {
    "GameCode": "Game_Test",
    "PlayerId": "123",
    "PlayerIP": "94.155.137.80",
    "CountryCode": "GB",
    "CurrencyCode": "GBP",
    "LanguageCode": "en-US",
    "Nickname": "",
    "HomeUrl": "https://operator.hub.stage.pariplaygames.com/Redirections/HomeUrl",
    "CashierUrl": "http://operator.hub.stage.pariplaygames.com/Redirections/CashierUrl",
    "Gender": 2,
    "PlayerRegulation": 2,
    "HistoryUrl": "http://operator.hub.stage.pariplaygames.com/Redirections/HistoryUrl",
    "IpAddress": "94.155.137.80",
    "IsMobile": False,
    "DebugMode": False,
    "IsNewWindow": False,
    "MiniGame": False,
    "Account": {"UserName": "t", "Password": "t"},
}

CreateToken_Request = {
    "Token": "Test_Token",
    "PlayerId": "Test_Player",
    "GameCode": "Game_Test",
    "Account": {"UserName": "USR", "Password": "PWD"},
}

Auth_Request = {
    "GameCode": "Game_Test",
    "ClientToken": "Client_Test",
    "Account": {"Username": "USR_Auth", "Password": "PWD_Auth"},
}

Debit_Request = {
    "Token": "Debit_Token",
    "GameCode": "Game_Test",
    "PlayerId": "Player_Id",
    "RoundId": "2564533",
    "TransactionId": "5364566",
    "Amount": 100.0,
    "EndGame": False,
    "Account": {"Username": "USR_Auth", "Password": "PWD_Auth"},
}

Credit_request = {
    "Token": "Credit_Token",
    "GameCode": "Game_Test",
    "PlayerId": "Player_Id",
    "RoundId": "2564533",
    "TransactionId": "5351926",
    "Amount": 100.0,
    "EndGame": False,
    "Account": {"Username": "USR_Auth", "Password": "PWD_Auth"},
}

Credit_request_EVI = {
    "Token": "Credit_Token",
    "GameCode": "Game_Test",
    "PlayerId": "Player_Id",
    "RoundId": "2564205",
    "TransactionId": "5364223",
    "DebitTransactionId": "1327593",
    "Amount": 100.0,
    "EndGame": False,
    "Account": {"Username": "USR_Auth", "Password": "PWD_Auth"},
}

CancelTransaction_Request = {
    "Token": "Cancel_TokenF",
    "TransactionId": "5364567",
    "RefTransactionId": "5364566",
    "PlayerId": "Player_Id",
    "RoundId": "2564533",
    "GameCode": "Game_Test",
    "EndGame": True,
    "CancelReason": "900",
    "Account": {"Username": "USR_Auth", "Password": "PWD_Auth"},
}

CancelTransaction_Request_EVI = {
    "DebitTransactionId": "Previous_Debit_Id",
    "Token": "Cancel_Token",
    "GameCode": "Game_Test",
    "PlayerId": "Player_Id",
    "TransactionId": "Unique_Id",
    "RoundId": "2564789",
    "IsRoundEnded": True,
    "Account": {"Username": "USR_Auth", "Password": "PWD_Auth"},
}
