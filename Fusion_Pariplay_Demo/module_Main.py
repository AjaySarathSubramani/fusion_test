import json
import logging
import re

import pytest_check as check
import requests


# To Write log in the Report
def log():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.setLevel(logging.ERROR)
    logger.setLevel(logging.INFO)
    pass


log()


def Response_Financial(req_name, Url, req_data):
    response = requests.post(Url, json=req_data)
    status_code = response.status_code
    check.equal(status_code, 200, "Fail : Incorrect Status Code")
    response = response.json()
    logging.info(req_name + " Request " + " : " + str(req_data))
    logging.info(req_name + " response " + " : " + str(response))
    logging.info(req_name + " Status " + " : " + str(status_code))
    return response


def readJsonFile(Request):
    json_input = json.dumps(Request)
    request_json = json.loads(json_input)
    return request_json


def Response_GameLaunch(arg_url, arg_req, arg, arg_PID):
    game_launch_req = readJsonFile(arg_req)
    game_launch_req["GameCode"] = arg
    game_launch_req["PlayerId"] = arg_PID
    game_launch_response = Response_Financial("GameLaunch", arg_url, game_launch_req)
    # print(game_launch_response)
    token = game_launch_response["Token"]
    url = game_launch_response["Url"]
    expression = "(?<=token=)([^&]*)(?=&)?"
    print(url)
    [client_token] = re.findall(expression, url)
    # Url_String = url.split("=", -1)
    # client_token = Url_String[-1]
    return token, url, client_token


def Response_CreateToken(arg_url, arg_req, arg, arg2, arg_P_Id, arg_user, arg_pwd):
    create_token_req = readJsonFile(arg_req)
    create_token_req["GameCode"] = arg
    create_token_req["Token"] = arg2
    create_token_req["PlayerId"] = arg_P_Id
    create_token_req["Account"]["UserName"] = arg_user
    create_token_req["Account"]["Password"] = arg_pwd
    create_token_response = Response_Financial("CreateToken", arg_url, create_token_req)
    print(create_token_response)
    error = create_token_response["Error"]
    check.equal(error, None, "error in the Create Token Response")


def Response_Authentication(arg_url, arg_req, arg, arg2, arg_user, arg_pwd):
    auth_req = readJsonFile(arg_req)
    auth_req["GameCode"] = arg
    auth_req["ClientToken"] = arg2
    auth_req["Account"]["Username"] = arg_user
    auth_req["Account"]["Password"] = arg_pwd
    print(auth_req)
    auth_response = Response_Financial("Authenticate ", arg_url, auth_req)
    print(auth_response)
    token = auth_response["Token"]
    player_id = auth_response["PlayerId"]
    # currency_code = auth_response["CurrencyCode"]
    # country_code = auth_response["CountryCode"]
    balance = auth_response["Balance"]
    return player_id, balance, token


def Response_Debit(
        arg_url,
        arg_req,
        arg_GameCode,
        arg_Token,
        arg_PID,
        arg_TID,
        arg_RID,
        arg_user,
        arg_pwd,
):
    debit_req = readJsonFile(arg_req)
    debit_req["RoundId"] = arg_RID
    debit_req["TransactionId"] = arg_TID
    debit_req["GameCode"] = arg_GameCode
    debit_req["Token"] = arg_Token
    debit_req["PlayerId"] = arg_PID
    debit_req["Account"]["Username"] = arg_user
    debit_req["Account"]["Password"] = arg_pwd
    debit_response = Response_Financial("Debit", arg_url, debit_req)
    balance = debit_response["Balance"]
    transaction_id = debit_response["TransactionId"]
    return balance, transaction_id


def Response_Credit(
        arg_url,
        arg_req,
        arg_GameCode,
        arg_Token,
        arg_DTID,
        arg_PID,
        arg_TID,
        arg_RID,
        arg_user,
        arg_pwd,
):
    credit_req = readJsonFile(arg_req)
    credit_req["RoundId"] = arg_RID
    credit_req["TransactionId"] = arg_TID
    credit_req["GameCode"] = arg_GameCode
    credit_req["Token"] = arg_Token
    credit_req["PlayerId"] = arg_PID
    credit_req["Account"]["Username"] = arg_user
    credit_req["Account"]["Password"] = arg_pwd
    if arg_user != "test":
        credit_req["DebitTransactionId"] = arg_DTID
    else:
        credit_req["RefTransactionId"] = arg_DTID
    credit_response = Response_Financial("Credit", arg_url, credit_req)
    balance = credit_response["Balance"]
    transaction_id = credit_response["TransactionId"]
    return balance, transaction_id


def Response_CancelTransaction(
        arg_url,
        arg_req,
        arg_Ref_T_Id,
        arg_GameCode,
        arg_Token,
        arg_PID,
        arg_TID,
        arg_RID,
        arg_user,
        arg_pwd,
):
    cancel_transaction_req = readJsonFile(arg_req)
    cancel_transaction_req["RoundId"] = arg_RID
    cancel_transaction_req["TransactionId"] = arg_TID
    cancel_transaction_req["GameCode"] = arg_GameCode
    cancel_transaction_req["Token"] = arg_Token
    cancel_transaction_req["PlayerId"] = arg_PID
    cancel_transaction_req["Account"]["Username"] = arg_user
    cancel_transaction_req["Account"]["Password"] = arg_pwd
    if arg_user != "test":
        cancel_transaction_req["DebitTransactionId"] = arg_Ref_T_Id
    else:
        cancel_transaction_req["RefTransactionId"] = arg_Ref_T_Id
    cancel_transaction_response = Response_Financial(
        "CancelTransaction", arg_url, cancel_transaction_req
    )
    balance = cancel_transaction_response["Balance"]
    transaction_id = cancel_transaction_response["TransactionId"]
    return balance, transaction_id
